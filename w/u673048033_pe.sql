-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 20, 2018 at 11:38 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u673048033_pe`
--

-- --------------------------------------------------------

--
-- Table structure for table `atividade`
--

CREATE TABLE `atividade` (
  `atividadeid` int(11) NOT NULL COMMENT 'Identificador',
  `atividadedescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `atividadeatividade` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atividade_conjatividade1`
--

CREATE TABLE `atividade_conjatividade1` (
  `atividade` int(11) NOT NULL,
  `conjatividade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atp`
--

CREATE TABLE `atp` (
  `atpid` int(11) NOT NULL COMMENT 'Identificador',
  `atpdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atvps`
--

CREATE TABLE `atvps` (
  `atvpsid` int(11) NOT NULL COMMENT 'Identificador',
  `atvpsdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `atvpssemanaid7` int(11) NOT NULL COMMENT 'FÃ³rum (OrientaÃ§Ã£o e SupervisÃ£o)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `atvssasemana`
--

CREATE TABLE `atvssasemana` (
  `atvssasemanaid` int(11) NOT NULL COMMENT 'Identificador',
  `atvssasemanadescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bibliografia`
--

CREATE TABLE `bibliografia` (
  `bibliografiaid` int(11) NOT NULL COMMENT 'Identificador',
  `bibliografiadescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bloco`
--

CREATE TABLE `bloco` (
  `blocoid` int(11) NOT NULL COMMENT 'Identificador',
  `blocobloco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'bloco'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `conjatividade`
--

CREATE TABLE `conjatividade` (
  `conjatividadeid` int(11) NOT NULL COMMENT 'Identificador',
  `conjatividadedescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `conjatividadeopsemanaid2` int(11) NOT NULL COMMENT 'Semana'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `conjatividade_atvssasemana1`
--

CREATE TABLE `conjatividade_atvssasemana1` (
  `conjatividade` int(11) NOT NULL,
  `atvssasemana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cp`
--

CREATE TABLE `cp` (
  `cpid` int(11) NOT NULL COMMENT 'Identificador',
  `cpnome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nome da unidade'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cp_planodeensino8`
--

CREATE TABLE `cp_planodeensino8` (
  `cp` int(11) NOT NULL,
  `planodeensino` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cronogramaav`
--

CREATE TABLE `cronogramaav` (
  `cronogramaavid` int(11) NOT NULL COMMENT 'Identificador',
  `cronogramaavn11` date DEFAULT NULL COMMENT 'N1.1',
  `cronogramaavn12` date DEFAULT NULL COMMENT 'N1.2',
  `cronogramaavn13` date DEFAULT NULL COMMENT 'N1.3',
  `cronogramaavn21` date DEFAULT NULL COMMENT 'N2.1',
  `cronogramaavn22` date DEFAULT NULL COMMENT 'N2.2',
  `cronogramaavn23` date DEFAULT NULL COMMENT 'N2.3',
  `cronogramaavn24` date DEFAULT NULL COMMENT 'N2.4',
  `cronogramaavn25` date DEFAULT NULL COMMENT 'N2.5',
  `cronogramaavdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `cronogramaavmetavaliativosid1` int(11) NOT NULL COMMENT 'N1.1 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid2` int(11) NOT NULL COMMENT 'N1.2 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid3` int(11) NOT NULL COMMENT 'N1.3 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid4` int(11) NOT NULL COMMENT 'N2.1 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid5` int(11) NOT NULL COMMENT 'N2.2 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid6` int(11) NOT NULL COMMENT 'N2.3 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid7` int(11) NOT NULL COMMENT 'N2.4 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid8` int(11) NOT NULL COMMENT 'N2.5 MÃ©todo de avaliaÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `curso`
--

CREATE TABLE `curso` (
  `cursoid` int(11) NOT NULL COMMENT 'Identificador',
  `cursocurso` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Curso'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `curso`
--

INSERT INTO `curso` (`cursoid`, `cursocurso`) VALUES
(1, 'FARMÁCIA'),
(2, 'ADMINISTRAÇÃO');

-- --------------------------------------------------------

--
-- Table structure for table `dificuldade`
--

CREATE TABLE `dificuldade` (
  `dificuldadeid` int(11) NOT NULL COMMENT 'Identificador',
  `dificuldadegrau` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Grau'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dificuldade`
--

INSERT INTO `dificuldade` (`dificuldadeid`, `dificuldadegrau`) VALUES
(1, 'Médio');

-- --------------------------------------------------------

--
-- Table structure for table `dificuldade_semana5`
--

CREATE TABLE `dificuldade_semana5` (
  `dificuldade` int(11) NOT NULL,
  `semana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `identificacao`
--

CREATE TABLE `identificacao` (
  `identificacaoid` int(11) NOT NULL COMMENT 'Identificador',
  `identificacaouc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Unidade Curricular ',
  `identificacaochtt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria Total (Horas)',
  `identificacaocht` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria TeÃ³rica (Horas/Aula)',
  `identificacaochp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria PrÃ¡tica l (Horas/Aula)',
  `identificacaochaol` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria AOL (Horas/Aula)',
  `identificacaochaps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria APS (Horas/Aula)',
  `identificacaoementa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'EMENTA',
  `identificacaoobjgeral` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'OBJETIVO GERAL',
  `identificacaousuarioid1` int(11) NOT NULL COMMENT 'Docente',
  `identificacaocursoid2` int(11) NOT NULL COMMENT 'Curso',
  `identificacaoturmaid3` int(11) NOT NULL COMMENT 'Turma'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `identificacao`
--

INSERT INTO `identificacao` (`identificacaoid`, `identificacaouc`, `identificacaochtt`, `identificacaocht`, `identificacaochp`, `identificacaochaol`, `identificacaochaps`, `identificacaoementa`, `identificacaoobjgeral`, `identificacaousuarioid1`, `identificacaocursoid2`, `identificacaoturmaid3`) VALUES
(1, 'Artur Romão Rocha', '3', '0', '0', '0', '0', '<p><br><br></p>', '<p>asasdfasfasdf</p><table class=\'table table-bordered\'><tbody><tr><td>as</td><td>a</td></tr><tr><td>as\'&nbsp;</td><td>as</td></tr><tr><td>\'</td><td><br></td></tr><tr><td><br></td><td><br></td></tr></tbody></table><p>asd</p>', 1, 2, 1),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(7, 'Coração', NULL, NULL, NULL, NULL, NULL, NULL, '<p>nkdnkdfngkdngdkfngdkngdkngdkdknwg10 n]´2-,ngreglg</p>', 1, 1, 1),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `itemeqt`
--

CREATE TABLE `itemeqt` (
  `itemeqtid` int(11) NOT NULL COMMENT 'Identificador',
  `itemeqtdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `itemeqtitem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Material',
  `itemeqtquantidade` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Quantidade'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `itemeqt_pap3`
--

CREATE TABLE `itemeqt_pap3` (
  `itemeqt` int(11) NOT NULL,
  `pap` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laboratorios`
--

CREATE TABLE `laboratorios` (
  `laboratoriosid` int(11) NOT NULL COMMENT 'Identificador',
  `laboratorioslab` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'LaboratÃ³rio',
  `laboratoriosdesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `laboratorios`
--

INSERT INTO `laboratorios` (`laboratoriosid`, `laboratorioslab`, `laboratoriosdesc`) VALUES
(1, 'Informática', ''),
(2, 'Informática', '');

-- --------------------------------------------------------

--
-- Table structure for table `metavaliativos`
--

CREATE TABLE `metavaliativos` (
  `metavaliativosid` int(11) NOT NULL COMMENT 'Identificador',
  `metavaliativosmetodo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'MÃ©todo',
  `metavaliativospeso` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Peso'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `metavaliativos`
--

INSERT INTO `metavaliativos` (`metavaliativosid`, `metavaliativosmetodo`, `metavaliativospeso`) VALUES
(1, 'Prova', '10');

-- --------------------------------------------------------

--
-- Table structure for table `nivelamento`
--

CREATE TABLE `nivelamento` (
  `nivelamentoid` int(11) NOT NULL COMMENT 'Identificador',
  `nivelamentodescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `nivelamentosemanaid2` int(11) NOT NULL COMMENT 'CHAT'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `opsemana`
--

CREATE TABLE `opsemana` (
  `opsemanaid` int(11) NOT NULL COMMENT 'Identificador',
  `opsemanaop` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Semana'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `opsemana_semana2`
--

CREATE TABLE `opsemana_semana2` (
  `opsemana` int(11) NOT NULL,
  `semana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `opsemana_semana4`
--

CREATE TABLE `opsemana_semana4` (
  `opsemana` int(11) NOT NULL,
  `semana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pap`
--

CREATE TABLE `pap` (
  `papid` int(11) NOT NULL COMMENT 'Identificador',
  `papnum` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'NÃšMERO DO PROTOCOLO',
  `papdia` date DEFAULT NULL COMMENT 'DATA',
  `paptitulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'TÃTULO',
  `papobj` text COLLATE utf8_unicode_ci,
  `papfunteo` text COLLATE utf8_unicode_ci,
  `pappromet` text COLLATE utf8_unicode_ci,
  `papresespvalref` text COLLATE utf8_unicode_ci,
  `papdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `papturnosid1` int(11) NOT NULL COMMENT 'TURNO DE EXECUÃ‡ÃƒO',
  `paplaboratoriosid2` int(11) NOT NULL COMMENT 'LABORATÃ“RIO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pap_planodeensino10`
--

CREATE TABLE `pap_planodeensino10` (
  `pap` int(11) NOT NULL,
  `planodeensino` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `planodeensino`
--

CREATE TABLE `planodeensino` (
  `planodeensinoid` int(11) NOT NULL COMMENT 'Identificador',
  `planodeensinodescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Descrição',
  `planodeensinodia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Data e hora',
  `planodeensinousuarioid1` int(11) DEFAULT NULL COMMENT 'Docente',
  `planodeensinocursoid2` int(11) DEFAULT NULL COMMENT 'Curso',
  `planodeensinoidentificacaoid3` int(11) DEFAULT NULL COMMENT 'Identificação',
  `planodeensinoatvpsid4` int(11) DEFAULT NULL COMMENT 'Atividade Prática Supervisionada',
  `planodeensinoprojetoid5` int(11) DEFAULT NULL COMMENT 'Projeto ou Plano de ação (Estágio)',
  `planodeensinonivelamentoid6` int(11) DEFAULT NULL COMMENT 'Nivelamento',
  `planodeensinobibliografiaid7` int(11) DEFAULT NULL COMMENT 'Bibliografia',
  `planodeensinoatpid9` int(11) DEFAULT NULL COMMENT 'Atividades previstas',
  `planodeensinocronogramaavid11` int(11) DEFAULT NULL COMMENT 'Cronograma de avaliação',
  `planodeensinoatvssasemanaid12` int(11) DEFAULT NULL COMMENT 'Semanas (Atividades Sagah, Siga ou SSA)',
  `planodeensinoativo` int(11) DEFAULT NULL COMMENT 'ativo?'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `planodeensino`
--

INSERT INTO `planodeensino` (`planodeensinoid`, `planodeensinodescricao`, `planodeensinodia`, `planodeensinousuarioid1`, `planodeensinocursoid2`, `planodeensinoidentificacaoid3`, `planodeensinoatvpsid4`, `planodeensinoprojetoid5`, `planodeensinonivelamentoid6`, `planodeensinobibliografiaid7`, `planodeensinoatpid9`, `planodeensinocronogramaavid11`, `planodeensinoatvssasemanaid12`, `planodeensinoativo`) VALUES
(1, 'Introdução a Informatica', NULL, 0, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'a', NULL, 0, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Anatomia', NULL, 0, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projeto`
--

CREATE TABLE `projeto` (
  `projetoid` int(11) NOT NULL COMMENT 'Identificador',
  `projetodescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `projetosemanaid1` int(11) NOT NULL COMMENT 'Itens',
  `projetosemanaid2` int(11) NOT NULL COMMENT 'CHAT'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prova`
--

CREATE TABLE `prova` (
  `provaid` int(11) NOT NULL COMMENT 'Identificador',
  `provaprova` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Prova'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `prova`
--

INSERT INTO `prova` (`provaid`, `provaprova`) VALUES
(1, 'Prova 1'),
(2, 'Prova 2');

-- --------------------------------------------------------

--
-- Table structure for table `prova_cp1`
--

CREATE TABLE `prova_cp1` (
  `prova` int(11) NOT NULL,
  `cp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `semana`
--

CREATE TABLE `semana` (
  `semanaid` int(11) NOT NULL COMMENT 'Identificador',
  `semanadescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `semanaatividade` text COLLATE utf8_unicode_ci,
  `semanablocoid1` int(11) NOT NULL COMMENT 'Bloco1',
  `semanablocoid3` int(11) NOT NULL COMMENT 'Bloco 2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `semana_atp1`
--

CREATE TABLE `semana_atp1` (
  `semana` int(11) NOT NULL,
  `atp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `semana_atvps1`
--

CREATE TABLE `semana_atvps1` (
  `semana` int(11) NOT NULL,
  `atvps` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `semana_bibliografia1`
--

CREATE TABLE `semana_bibliografia1` (
  `semana` int(11) NOT NULL,
  `bibliografia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `semana_bibliografia2`
--

CREATE TABLE `semana_bibliografia2` (
  `semana` int(11) NOT NULL,
  `bibliografia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `semana_bibliografia3`
--

CREATE TABLE `semana_bibliografia3` (
  `semana` int(11) NOT NULL,
  `bibliografia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `semana_cp2`
--

CREATE TABLE `semana_cp2` (
  `semana` int(11) NOT NULL,
  `cp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `semana_nivelamento1`
--

CREATE TABLE `semana_nivelamento1` (
  `semana` int(11) NOT NULL,
  `nivelamento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tempos`
--

CREATE TABLE `tempos` (
  `temposid` int(11) NOT NULL COMMENT 'Identificador',
  `temposdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `tempostempo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Tempo (1 Âª...)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tempos_turnos1`
--

CREATE TABLE `tempos_turnos1` (
  `tempos` int(11) NOT NULL,
  `turnos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tempos_turnos2`
--

CREATE TABLE `tempos_turnos2` (
  `tempos` int(11) NOT NULL,
  `turnos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tempos_turnos3`
--

CREATE TABLE `tempos_turnos3` (
  `tempos` int(11) NOT NULL,
  `turnos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `turma`
--

CREATE TABLE `turma` (
  `turmaid` int(11) NOT NULL COMMENT 'Identificador',
  `turmaturma` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Turma'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `turma`
--

INSERT INTO `turma` (`turmaid`, `turmaturma`) VALUES
(1, '2018/1');

-- --------------------------------------------------------

--
-- Table structure for table `turnos`
--

CREATE TABLE `turnos` (
  `turnosid` int(11) NOT NULL COMMENT 'Identificador',
  `turnosdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `usuarioid` int(11) NOT NULL COMMENT 'Identificador',
  `usuarionome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nome',
  `usuarioemail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'E-MAIL',
  `usuariosenha` text COLLATE utf8_unicode_ci,
  `usuariomatricula` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'matricula'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`usuarioid`, `usuarionome`, `usuarioemail`, `usuariosenha`, `usuariomatricula`) VALUES
(1, 'Artur Romão Rocha', 'arturromaorocha@gmail.com', '223311', '29375');

-- --------------------------------------------------------

--
-- Table structure for table `verbos`
--

CREATE TABLE `verbos` (
  `verbosid` int(11) NOT NULL COMMENT 'Identificador',
  `verbosverbo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Verbo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `verbos`
--

INSERT INTO `verbos` (`verbosid`, `verbosverbo`) VALUES
(1, 'Verbo 1');

-- --------------------------------------------------------

--
-- Table structure for table `verbos_dificuldade1`
--

CREATE TABLE `verbos_dificuldade1` (
  `verbos` int(11) NOT NULL,
  `dificuldade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `verbos_semana6`
--

CREATE TABLE `verbos_semana6` (
  `verbos` int(11) NOT NULL,
  `semana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atividade`
--
ALTER TABLE `atividade`
  ADD PRIMARY KEY (`atividadeid`);

--
-- Indexes for table `atividade_conjatividade1`
--
ALTER TABLE `atividade_conjatividade1`
  ADD PRIMARY KEY (`atividade`,`conjatividade`),
  ADD KEY `fk_conjatividadeatividadeconjatividade1` (`conjatividade`);

--
-- Indexes for table `atp`
--
ALTER TABLE `atp`
  ADD PRIMARY KEY (`atpid`);

--
-- Indexes for table `atvps`
--
ALTER TABLE `atvps`
  ADD PRIMARY KEY (`atvpsid`,`atvpssemanaid7`),
  ADD KEY `fk_atvpssemana7` (`atvpssemanaid7`);

--
-- Indexes for table `atvssasemana`
--
ALTER TABLE `atvssasemana`
  ADD PRIMARY KEY (`atvssasemanaid`);

--
-- Indexes for table `bibliografia`
--
ALTER TABLE `bibliografia`
  ADD PRIMARY KEY (`bibliografiaid`);

--
-- Indexes for table `bloco`
--
ALTER TABLE `bloco`
  ADD PRIMARY KEY (`blocoid`);

--
-- Indexes for table `conjatividade`
--
ALTER TABLE `conjatividade`
  ADD PRIMARY KEY (`conjatividadeid`,`conjatividadeopsemanaid2`),
  ADD KEY `fk_conjatividadeopsemana2` (`conjatividadeopsemanaid2`);

--
-- Indexes for table `conjatividade_atvssasemana1`
--
ALTER TABLE `conjatividade_atvssasemana1`
  ADD PRIMARY KEY (`conjatividade`,`atvssasemana`),
  ADD KEY `fk_atvssasemanaconjatividadeatvssasemana1` (`atvssasemana`);

--
-- Indexes for table `cp`
--
ALTER TABLE `cp`
  ADD PRIMARY KEY (`cpid`);

--
-- Indexes for table `cp_planodeensino8`
--
ALTER TABLE `cp_planodeensino8`
  ADD PRIMARY KEY (`cp`,`planodeensino`),
  ADD KEY `fk_planodeensinocpplanodeensino8` (`planodeensino`);

--
-- Indexes for table `cronogramaav`
--
ALTER TABLE `cronogramaav`
  ADD PRIMARY KEY (`cronogramaavid`,`cronogramaavmetavaliativosid1`,`cronogramaavmetavaliativosid2`,`cronogramaavmetavaliativosid3`,`cronogramaavmetavaliativosid4`,`cronogramaavmetavaliativosid5`,`cronogramaavmetavaliativosid6`,`cronogramaavmetavaliativosid7`,`cronogramaavmetavaliativosid8`),
  ADD KEY `fk_cronogramaavmetavaliativos1` (`cronogramaavmetavaliativosid1`),
  ADD KEY `fk_cronogramaavmetavaliativos2` (`cronogramaavmetavaliativosid2`),
  ADD KEY `fk_cronogramaavmetavaliativos3` (`cronogramaavmetavaliativosid3`),
  ADD KEY `fk_cronogramaavmetavaliativos4` (`cronogramaavmetavaliativosid4`),
  ADD KEY `fk_cronogramaavmetavaliativos5` (`cronogramaavmetavaliativosid5`),
  ADD KEY `fk_cronogramaavmetavaliativos6` (`cronogramaavmetavaliativosid6`),
  ADD KEY `fk_cronogramaavmetavaliativos7` (`cronogramaavmetavaliativosid7`),
  ADD KEY `fk_cronogramaavmetavaliativos8` (`cronogramaavmetavaliativosid8`);

--
-- Indexes for table `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`cursoid`);

--
-- Indexes for table `dificuldade`
--
ALTER TABLE `dificuldade`
  ADD PRIMARY KEY (`dificuldadeid`);

--
-- Indexes for table `dificuldade_semana5`
--
ALTER TABLE `dificuldade_semana5`
  ADD PRIMARY KEY (`dificuldade`,`semana`),
  ADD KEY `fk_semanadificuldadesemana5` (`semana`);

--
-- Indexes for table `identificacao`
--
ALTER TABLE `identificacao`
  ADD PRIMARY KEY (`identificacaoid`,`identificacaousuarioid1`,`identificacaocursoid2`,`identificacaoturmaid3`),
  ADD KEY `fk_identificacaousuario1` (`identificacaousuarioid1`),
  ADD KEY `fk_identificacaocurso2` (`identificacaocursoid2`),
  ADD KEY `fk_identificacaoturma3` (`identificacaoturmaid3`);

--
-- Indexes for table `itemeqt`
--
ALTER TABLE `itemeqt`
  ADD PRIMARY KEY (`itemeqtid`);

--
-- Indexes for table `itemeqt_pap3`
--
ALTER TABLE `itemeqt_pap3`
  ADD PRIMARY KEY (`itemeqt`,`pap`),
  ADD KEY `fk_papitemeqtpap3` (`pap`);

--
-- Indexes for table `laboratorios`
--
ALTER TABLE `laboratorios`
  ADD PRIMARY KEY (`laboratoriosid`);

--
-- Indexes for table `metavaliativos`
--
ALTER TABLE `metavaliativos`
  ADD PRIMARY KEY (`metavaliativosid`);

--
-- Indexes for table `nivelamento`
--
ALTER TABLE `nivelamento`
  ADD PRIMARY KEY (`nivelamentoid`,`nivelamentosemanaid2`),
  ADD KEY `fk_nivelamentosemana2` (`nivelamentosemanaid2`);

--
-- Indexes for table `opsemana`
--
ALTER TABLE `opsemana`
  ADD PRIMARY KEY (`opsemanaid`);

--
-- Indexes for table `opsemana_semana2`
--
ALTER TABLE `opsemana_semana2`
  ADD PRIMARY KEY (`opsemana`,`semana`),
  ADD KEY `fk_semanaopsemanasemana2` (`semana`);

--
-- Indexes for table `opsemana_semana4`
--
ALTER TABLE `opsemana_semana4`
  ADD PRIMARY KEY (`opsemana`,`semana`),
  ADD KEY `fk_semanaopsemanasemana4` (`semana`);

--
-- Indexes for table `pap`
--
ALTER TABLE `pap`
  ADD PRIMARY KEY (`papid`,`papturnosid1`,`paplaboratoriosid2`),
  ADD KEY `fk_papturnos1` (`papturnosid1`),
  ADD KEY `fk_paplaboratorios2` (`paplaboratoriosid2`);

--
-- Indexes for table `pap_planodeensino10`
--
ALTER TABLE `pap_planodeensino10`
  ADD PRIMARY KEY (`pap`,`planodeensino`),
  ADD KEY `fk_planodeensinopapplanodeensino10` (`planodeensino`);

--
-- Indexes for table `planodeensino`
--
ALTER TABLE `planodeensino`
  ADD PRIMARY KEY (`planodeensinoid`);

--
-- Indexes for table `projeto`
--
ALTER TABLE `projeto`
  ADD PRIMARY KEY (`projetoid`,`projetosemanaid1`,`projetosemanaid2`),
  ADD KEY `fk_projetosemana1` (`projetosemanaid1`),
  ADD KEY `fk_projetosemana2` (`projetosemanaid2`);

--
-- Indexes for table `prova`
--
ALTER TABLE `prova`
  ADD PRIMARY KEY (`provaid`);

--
-- Indexes for table `prova_cp1`
--
ALTER TABLE `prova_cp1`
  ADD PRIMARY KEY (`prova`,`cp`),
  ADD KEY `fk_cpprovacp1` (`cp`);

--
-- Indexes for table `semana`
--
ALTER TABLE `semana`
  ADD PRIMARY KEY (`semanaid`,`semanablocoid1`,`semanablocoid3`),
  ADD KEY `fk_semanabloco1` (`semanablocoid1`),
  ADD KEY `fk_semanabloco3` (`semanablocoid3`);

--
-- Indexes for table `semana_atp1`
--
ALTER TABLE `semana_atp1`
  ADD PRIMARY KEY (`semana`,`atp`),
  ADD KEY `fk_atpsemanaatp1` (`atp`);

--
-- Indexes for table `semana_atvps1`
--
ALTER TABLE `semana_atvps1`
  ADD PRIMARY KEY (`semana`,`atvps`),
  ADD KEY `fk_atvpssemanaatvps1` (`atvps`);

--
-- Indexes for table `semana_bibliografia1`
--
ALTER TABLE `semana_bibliografia1`
  ADD PRIMARY KEY (`semana`,`bibliografia`),
  ADD KEY `fk_bibliografiasemanabibliografia1` (`bibliografia`);

--
-- Indexes for table `semana_bibliografia2`
--
ALTER TABLE `semana_bibliografia2`
  ADD PRIMARY KEY (`semana`,`bibliografia`),
  ADD KEY `fk_bibliografiasemanabibliografia2` (`bibliografia`);

--
-- Indexes for table `semana_bibliografia3`
--
ALTER TABLE `semana_bibliografia3`
  ADD PRIMARY KEY (`semana`,`bibliografia`),
  ADD KEY `fk_bibliografiasemanabibliografia3` (`bibliografia`);

--
-- Indexes for table `semana_cp2`
--
ALTER TABLE `semana_cp2`
  ADD PRIMARY KEY (`semana`,`cp`),
  ADD KEY `fk_cpsemanacp2` (`cp`);

--
-- Indexes for table `semana_nivelamento1`
--
ALTER TABLE `semana_nivelamento1`
  ADD PRIMARY KEY (`semana`,`nivelamento`),
  ADD KEY `fk_nivelamentosemananivelamento1` (`nivelamento`);

--
-- Indexes for table `tempos`
--
ALTER TABLE `tempos`
  ADD PRIMARY KEY (`temposid`);

--
-- Indexes for table `tempos_turnos1`
--
ALTER TABLE `tempos_turnos1`
  ADD PRIMARY KEY (`tempos`,`turnos`),
  ADD KEY `fk_turnostemposturnos1` (`turnos`);

--
-- Indexes for table `tempos_turnos2`
--
ALTER TABLE `tempos_turnos2`
  ADD PRIMARY KEY (`tempos`,`turnos`),
  ADD KEY `fk_turnostemposturnos2` (`turnos`);

--
-- Indexes for table `tempos_turnos3`
--
ALTER TABLE `tempos_turnos3`
  ADD PRIMARY KEY (`tempos`,`turnos`),
  ADD KEY `fk_turnostemposturnos3` (`turnos`);

--
-- Indexes for table `turma`
--
ALTER TABLE `turma`
  ADD PRIMARY KEY (`turmaid`);

--
-- Indexes for table `turnos`
--
ALTER TABLE `turnos`
  ADD PRIMARY KEY (`turnosid`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuarioid`);

--
-- Indexes for table `verbos`
--
ALTER TABLE `verbos`
  ADD PRIMARY KEY (`verbosid`);

--
-- Indexes for table `verbos_dificuldade1`
--
ALTER TABLE `verbos_dificuldade1`
  ADD PRIMARY KEY (`verbos`,`dificuldade`),
  ADD KEY `fk_dificuldadeverbosdificuldade1` (`dificuldade`);

--
-- Indexes for table `verbos_semana6`
--
ALTER TABLE `verbos_semana6`
  ADD PRIMARY KEY (`verbos`,`semana`),
  ADD KEY `fk_semanaverbossemana6` (`semana`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atividade`
--
ALTER TABLE `atividade`
  MODIFY `atividadeid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `atp`
--
ALTER TABLE `atp`
  MODIFY `atpid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `atvps`
--
ALTER TABLE `atvps`
  MODIFY `atvpsid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `atvssasemana`
--
ALTER TABLE `atvssasemana`
  MODIFY `atvssasemanaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `bibliografia`
--
ALTER TABLE `bibliografia`
  MODIFY `bibliografiaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `bloco`
--
ALTER TABLE `bloco`
  MODIFY `blocoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `conjatividade`
--
ALTER TABLE `conjatividade`
  MODIFY `conjatividadeid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `cp`
--
ALTER TABLE `cp`
  MODIFY `cpid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `cronogramaav`
--
ALTER TABLE `cronogramaav`
  MODIFY `cronogramaavid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `curso`
--
ALTER TABLE `curso`
  MODIFY `cursoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `dificuldade`
--
ALTER TABLE `dificuldade`
  MODIFY `dificuldadeid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `identificacao`
--
ALTER TABLE `identificacao`
  MODIFY `identificacaoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `itemeqt`
--
ALTER TABLE `itemeqt`
  MODIFY `itemeqtid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `laboratorios`
--
ALTER TABLE `laboratorios`
  MODIFY `laboratoriosid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `metavaliativos`
--
ALTER TABLE `metavaliativos`
  MODIFY `metavaliativosid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nivelamento`
--
ALTER TABLE `nivelamento`
  MODIFY `nivelamentoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `opsemana`
--
ALTER TABLE `opsemana`
  MODIFY `opsemanaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `pap`
--
ALTER TABLE `pap`
  MODIFY `papid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `planodeensino`
--
ALTER TABLE `planodeensino`
  MODIFY `planodeensinoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `projeto`
--
ALTER TABLE `projeto`
  MODIFY `projetoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `prova`
--
ALTER TABLE `prova`
  MODIFY `provaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `semana`
--
ALTER TABLE `semana`
  MODIFY `semanaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `tempos`
--
ALTER TABLE `tempos`
  MODIFY `temposid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `turma`
--
ALTER TABLE `turma`
  MODIFY `turmaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `turnos`
--
ALTER TABLE `turnos`
  MODIFY `turnosid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuarioid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `verbos`
--
ALTER TABLE `verbos`
  MODIFY `verbosid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `atividade_conjatividade1`
--
ALTER TABLE `atividade_conjatividade1`
  ADD CONSTRAINT `fk_conjatividadeatividadeatividade1` FOREIGN KEY (`atividade`) REFERENCES `atividade` (`atividadeid`),
  ADD CONSTRAINT `fk_conjatividadeatividadeconjatividade1` FOREIGN KEY (`conjatividade`) REFERENCES `conjatividade` (`conjatividadeid`);

--
-- Constraints for table `atvps`
--
ALTER TABLE `atvps`
  ADD CONSTRAINT `fk_atvpssemana7` FOREIGN KEY (`atvpssemanaid7`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `conjatividade`
--
ALTER TABLE `conjatividade`
  ADD CONSTRAINT `fk_conjatividadeopsemana2` FOREIGN KEY (`conjatividadeopsemanaid2`) REFERENCES `opsemana` (`opsemanaid`);

--
-- Constraints for table `conjatividade_atvssasemana1`
--
ALTER TABLE `conjatividade_atvssasemana1`
  ADD CONSTRAINT `fk_atvssasemanaconjatividadeatvssasemana1` FOREIGN KEY (`atvssasemana`) REFERENCES `atvssasemana` (`atvssasemanaid`),
  ADD CONSTRAINT `fk_atvssasemanaconjatividadeconjatividade1` FOREIGN KEY (`conjatividade`) REFERENCES `conjatividade` (`conjatividadeid`);

--
-- Constraints for table `cp_planodeensino8`
--
ALTER TABLE `cp_planodeensino8`
  ADD CONSTRAINT `fk_planodeensinocpcp8` FOREIGN KEY (`cp`) REFERENCES `cp` (`cpid`),
  ADD CONSTRAINT `fk_planodeensinocpplanodeensino8` FOREIGN KEY (`planodeensino`) REFERENCES `planodeensino` (`planodeensinoid`);

--
-- Constraints for table `cronogramaav`
--
ALTER TABLE `cronogramaav`
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos1` FOREIGN KEY (`cronogramaavmetavaliativosid1`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos2` FOREIGN KEY (`cronogramaavmetavaliativosid2`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos3` FOREIGN KEY (`cronogramaavmetavaliativosid3`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos4` FOREIGN KEY (`cronogramaavmetavaliativosid4`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos5` FOREIGN KEY (`cronogramaavmetavaliativosid5`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos6` FOREIGN KEY (`cronogramaavmetavaliativosid6`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos7` FOREIGN KEY (`cronogramaavmetavaliativosid7`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos8` FOREIGN KEY (`cronogramaavmetavaliativosid8`) REFERENCES `metavaliativos` (`metavaliativosid`);

--
-- Constraints for table `dificuldade_semana5`
--
ALTER TABLE `dificuldade_semana5`
  ADD CONSTRAINT `fk_semanadificuldadedificuldade5` FOREIGN KEY (`dificuldade`) REFERENCES `dificuldade` (`dificuldadeid`),
  ADD CONSTRAINT `fk_semanadificuldadesemana5` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `identificacao`
--
ALTER TABLE `identificacao`
  ADD CONSTRAINT `fk_identificacaocurso2` FOREIGN KEY (`identificacaocursoid2`) REFERENCES `curso` (`cursoid`),
  ADD CONSTRAINT `fk_identificacaoturma3` FOREIGN KEY (`identificacaoturmaid3`) REFERENCES `turma` (`turmaid`),
  ADD CONSTRAINT `fk_identificacaousuario1` FOREIGN KEY (`identificacaousuarioid1`) REFERENCES `usuario` (`usuarioid`);

--
-- Constraints for table `itemeqt_pap3`
--
ALTER TABLE `itemeqt_pap3`
  ADD CONSTRAINT `fk_papitemeqtitemeqt3` FOREIGN KEY (`itemeqt`) REFERENCES `itemeqt` (`itemeqtid`),
  ADD CONSTRAINT `fk_papitemeqtpap3` FOREIGN KEY (`pap`) REFERENCES `pap` (`papid`);

--
-- Constraints for table `nivelamento`
--
ALTER TABLE `nivelamento`
  ADD CONSTRAINT `fk_nivelamentosemana2` FOREIGN KEY (`nivelamentosemanaid2`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `opsemana_semana2`
--
ALTER TABLE `opsemana_semana2`
  ADD CONSTRAINT `fk_semanaopsemanaopsemana2` FOREIGN KEY (`opsemana`) REFERENCES `opsemana` (`opsemanaid`),
  ADD CONSTRAINT `fk_semanaopsemanasemana2` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `opsemana_semana4`
--
ALTER TABLE `opsemana_semana4`
  ADD CONSTRAINT `fk_semanaopsemanaopsemana4` FOREIGN KEY (`opsemana`) REFERENCES `opsemana` (`opsemanaid`),
  ADD CONSTRAINT `fk_semanaopsemanasemana4` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `pap`
--
ALTER TABLE `pap`
  ADD CONSTRAINT `fk_paplaboratorios2` FOREIGN KEY (`paplaboratoriosid2`) REFERENCES `laboratorios` (`laboratoriosid`),
  ADD CONSTRAINT `fk_papturnos1` FOREIGN KEY (`papturnosid1`) REFERENCES `turnos` (`turnosid`);

--
-- Constraints for table `pap_planodeensino10`
--
ALTER TABLE `pap_planodeensino10`
  ADD CONSTRAINT `fk_planodeensinopappap10` FOREIGN KEY (`pap`) REFERENCES `pap` (`papid`),
  ADD CONSTRAINT `fk_planodeensinopapplanodeensino10` FOREIGN KEY (`planodeensino`) REFERENCES `planodeensino` (`planodeensinoid`);

--
-- Constraints for table `projeto`
--
ALTER TABLE `projeto`
  ADD CONSTRAINT `fk_projetosemana1` FOREIGN KEY (`projetosemanaid1`) REFERENCES `semana` (`semanaid`),
  ADD CONSTRAINT `fk_projetosemana2` FOREIGN KEY (`projetosemanaid2`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `prova_cp1`
--
ALTER TABLE `prova_cp1`
  ADD CONSTRAINT `fk_cpprovacp1` FOREIGN KEY (`cp`) REFERENCES `cp` (`cpid`),
  ADD CONSTRAINT `fk_cpprovaprova1` FOREIGN KEY (`prova`) REFERENCES `prova` (`provaid`);

--
-- Constraints for table `semana`
--
ALTER TABLE `semana`
  ADD CONSTRAINT `fk_semanabloco1` FOREIGN KEY (`semanablocoid1`) REFERENCES `bloco` (`blocoid`),
  ADD CONSTRAINT `fk_semanabloco3` FOREIGN KEY (`semanablocoid3`) REFERENCES `bloco` (`blocoid`);

--
-- Constraints for table `semana_atp1`
--
ALTER TABLE `semana_atp1`
  ADD CONSTRAINT `fk_atpsemanaatp1` FOREIGN KEY (`atp`) REFERENCES `atp` (`atpid`),
  ADD CONSTRAINT `fk_atpsemanasemana1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `semana_atvps1`
--
ALTER TABLE `semana_atvps1`
  ADD CONSTRAINT `fk_atvpssemanaatvps1` FOREIGN KEY (`atvps`) REFERENCES `atvps` (`atvpsid`),
  ADD CONSTRAINT `fk_atvpssemanasemana1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `semana_bibliografia1`
--
ALTER TABLE `semana_bibliografia1`
  ADD CONSTRAINT `fk_bibliografiasemanabibliografia1` FOREIGN KEY (`bibliografia`) REFERENCES `bibliografia` (`bibliografiaid`),
  ADD CONSTRAINT `fk_bibliografiasemanasemana1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `semana_bibliografia2`
--
ALTER TABLE `semana_bibliografia2`
  ADD CONSTRAINT `fk_bibliografiasemanabibliografia2` FOREIGN KEY (`bibliografia`) REFERENCES `bibliografia` (`bibliografiaid`),
  ADD CONSTRAINT `fk_bibliografiasemanasemana2` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `semana_bibliografia3`
--
ALTER TABLE `semana_bibliografia3`
  ADD CONSTRAINT `fk_bibliografiasemanabibliografia3` FOREIGN KEY (`bibliografia`) REFERENCES `bibliografia` (`bibliografiaid`),
  ADD CONSTRAINT `fk_bibliografiasemanasemana3` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `semana_cp2`
--
ALTER TABLE `semana_cp2`
  ADD CONSTRAINT `fk_cpsemanacp2` FOREIGN KEY (`cp`) REFERENCES `cp` (`cpid`),
  ADD CONSTRAINT `fk_cpsemanasemana2` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `semana_nivelamento1`
--
ALTER TABLE `semana_nivelamento1`
  ADD CONSTRAINT `fk_nivelamentosemananivelamento1` FOREIGN KEY (`nivelamento`) REFERENCES `nivelamento` (`nivelamentoid`),
  ADD CONSTRAINT `fk_nivelamentosemanasemana1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Constraints for table `tempos_turnos1`
--
ALTER TABLE `tempos_turnos1`
  ADD CONSTRAINT `fk_turnostempostempos1` FOREIGN KEY (`tempos`) REFERENCES `tempos` (`temposid`),
  ADD CONSTRAINT `fk_turnostemposturnos1` FOREIGN KEY (`turnos`) REFERENCES `turnos` (`turnosid`);

--
-- Constraints for table `tempos_turnos2`
--
ALTER TABLE `tempos_turnos2`
  ADD CONSTRAINT `fk_turnostempostempos2` FOREIGN KEY (`tempos`) REFERENCES `tempos` (`temposid`),
  ADD CONSTRAINT `fk_turnostemposturnos2` FOREIGN KEY (`turnos`) REFERENCES `turnos` (`turnosid`);

--
-- Constraints for table `tempos_turnos3`
--
ALTER TABLE `tempos_turnos3`
  ADD CONSTRAINT `fk_turnostempostempos3` FOREIGN KEY (`tempos`) REFERENCES `tempos` (`temposid`),
  ADD CONSTRAINT `fk_turnostemposturnos3` FOREIGN KEY (`turnos`) REFERENCES `turnos` (`turnosid`);

--
-- Constraints for table `verbos_dificuldade1`
--
ALTER TABLE `verbos_dificuldade1`
  ADD CONSTRAINT `fk_dificuldadeverbosdificuldade1` FOREIGN KEY (`dificuldade`) REFERENCES `dificuldade` (`dificuldadeid`),
  ADD CONSTRAINT `fk_dificuldadeverbosverbos1` FOREIGN KEY (`verbos`) REFERENCES `verbos` (`verbosid`);

--
-- Constraints for table `verbos_semana6`
--
ALTER TABLE `verbos_semana6`
  ADD CONSTRAINT `fk_semanaverbossemana6` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`),
  ADD CONSTRAINT `fk_semanaverbosverbos6` FOREIGN KEY (`verbos`) REFERENCES `verbos` (`verbosid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
