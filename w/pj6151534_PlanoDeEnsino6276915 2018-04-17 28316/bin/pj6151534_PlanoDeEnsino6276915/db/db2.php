<?php

/**
 * Description of bd
 *
 * @author artur
 */
class bd2 {

    private $user = "postgres"; // root
    private $pass = "postgres"; // servidor
    private $host = "localhost"; // em que maquina esta o BD
    private $port="3536"; //porta 
    private $database = "saturno"; // nome do BD lojavirtual 40.41 db2.sql
    private $maxPage = 100; //aqui é o maximo de registo consultados por vez.
    private $pageNum;
    private $inicioBusca;
    private $totalPaginas;
    private $total;
    private $rs;
    private $x;
    private $sql;
 

    
    public function retorno($r) {

        switch ($r) {
            case 'add':
                echo ' <div class="aviso col-lg-12 col-sm-12 col-md-12 col-xs-12" style="position:fixed; top: 55px; z-index: 49;">
                              <div class="alert alert-dismissible alert-success">
                               <button type="button" onclick="$(' . "'.aviso'" . ').remove()" class="close" data-dismiss="alert">&times;</button>
                               <strong>Alerta</strong> cadastro efetuado com sucesso!
                            </div>   </div>
                         ';
                break;
            case 'del':
                echo ' <div class="aviso col-lg-12 col-sm-12 col-md-12 col-xs-12" style="position:fixed; top: 55px; z-index: 50;">
                              <div class="alert alert-dismissible alert-danger">
                               <button type="button" onclick="$(' . "'.aviso'" . ').remove()" class="close" data-dismiss="alert">&times;</button>
                               <strong>Alerta</strong> cadastro deletado com sucesso!
                            </div>   </div>
                           ';
                break;
            case 'edt':
                echo ' <div class="aviso col-lg-12 col-sm-12 col-md-12 col-xs-12" style="position:fixed; top: 55px; z-index: 51;">
                              <div class="alert alert-dismissible alert-warning">
                               <button type="button" onclick="$(' . "'.aviso'" . ').remove()" class="close" data-dismiss="alert">&times;</button>
                               <strong>Alerta</strong> cadastro editado com sucesso!
                            </div>   </div>
                       ';
                break;

            default:
                break;
        }
    }
    private function Connect() {
         
        $conn = new PDO("pgsql:host=$this->host port=$this->port dbname=$this->database", $this->user, $this->pass);
        
        return $conn;
    }

    function getUser() {
        return $this->user;
    }

    function getPass() {
        return $this->pass;
    }

    function getHost() {
        return $this->host;
    }

    function getDatabase() {
        return $this->database;
    }

    function getRs() {
        return $this->rs;
    }

    function getX() {
        return $this->x;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function setPass($pass) {
        $this->pass = $pass;
    }

    function setHost($host) {
        $this->host = $host;
    }

    function setDatabase($database) {
        $this->database = $database;
    }

    function setRs($rs) {
        $this->rs = $rs;
    }

    function setX($x) {
        $this->x = $x;
    }

    public function getMaxPage() {
        return $this->maxPage;
    }

    public function gettotal() {
        return $this->total;
    }

    public function getPageNum() {
        return $this->pageNum;
    }

    public function getInicioBusca() {
        return $this->inicioBusca;
    }

    public function getTotalPaginas() {
        return $this->totalPaginas;
    }

    public function setMaxPage($maxPage) {
        $this->maxPage = $maxPage;
    }

    public function setPageNum($pageNum) {
        $this->pageNum = $pageNum;
    }

    public function setInicioBusca($inicioBusca) {
        $this->inicioBusca = $inicioBusca;
    }

    public function setTotalPaginas($totalPaginas) {
        $this->totalPaginas = $totalPaginas;
    }

    public function RunQuery($sql) {
        $this->sql = $sql;
        $stm = $this->Connect()->prepare($this->sql);
        return $stm->execute();
    }

    public function RunSelect($sql) {
         $this->sql = $sql;
        $stm = $this->Connect()->prepare($this->sql);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function dateToBR($dataAmericana) {
        $d = explode('-', $dataAmericana);
        $dOK = $d[2] . '/' . $d[1] . '/' . $d[0];
        return $dOK;
    }

    public function dateToUS($dataBrasil) {
        $d = explode('/', $dataBrasil);
        $dOK = $d[2] . '-' . $d[1] . '-' . $d[0];
        return $dOK;
    }

    public function dateTimeToBR($data_americana_his) {
        $d = explode(' ', $data_americana_his);
        $ok = $this->dateToBR($d[0]) . ' ' . $d[1];
        return $ok;
    }

    public function dateTimeToUS($data_br_his) {
        $d = explode(' ', $data_br_his);
        $ok = $this->dateToUS($d[0]) . ' ' . $d[1];
        return $ok;
    }

    public function adicionar($tabela, $campo, $valor) {
         $this->sql = "INSERT INTO $tabela ($campo) VALUES ($valor)";
        $this->RunQuery($this->sql);
        
        $this->retorno('add');
    }

    public function deletar($id, $tabela, $campochaveID) {
        $this->sql = "delete from $tabela where $campochaveID = " . $id;
        $this->RunSelect($this->sql);
        
        $this->retorno('del');
    }

    public function editar($tabela, $valores, $id, $campoChaveID) {
        $this->sql = "UPDATE $tabela SET $valores WHERE $campoChaveID =$id";
        $this->RunQuery($this->sql);
        
        $this->retorno('edt');
    }

    public function listaTodos($tabela, $campochave,$l=true) {
        $var = '';
        $this->pageNum = 0;
        if (isset($_GET['page'])) {
            $this->pageNum = $_GET['page'] - 1;
        }
        $this->inicioBusca = $this->pageNum * $this->maxPage;
        if (@$_GET['busca'] == 'true' and $_GET['valor'] != '') {
            $_GET['valor'] = $this->noSql($_GET['valor']);
            $x = explode(' ', $_GET['valor']);
            foreach ($x as $x) {
                $var.="UPPER($campochave) like UPPER('%" . $x . "%')  or  ";
            }
            $temp = str_replace("'", '"', $this->sql);
            $var.=" UPPER($campochave) like UPPER('%" . $_GET['valor'] . "%')  and ";
            $this->sql = "SELECT * FROM $tabela where $var 1=1  order by $campochave ";
       $sql=     $this->sql2 = $this->sql;
         $sql = sprintf("%s LIMIT %s OFFSET %s", $this->sql, $this->maxPage, $this->inicioBusca);
        } else {
            $this->sql = "SELECT * FROM $tabela order by $campochave ";
       $sql=     $this->sql2 = $this->sql;
       if ($l === true) {
          $sql = sprintf("%s LIMIT %d OFFSET %d", $this->sql, $this->maxPage, $this->inicioBusca);
       }else {
            $sql= $this->sql;    
            }
          }

        $this->x = $this->RunSelect($this->sql2);
        $this->setTotal();
        $this->totalPaginas = ceil($this->total / $this->maxPage);
        $this->rs = $this->RunSelect($sql);
        return $this->rs;
    }

    public function listaOR($id, $tabela, $campochaveID) {
        $this->sql = sprintf("select * from $tabela order by $campochaveID = %s desc", $id);
        $this->rs = $this->RunSelect($this->sql);
        return $this->rs;
    }

    public function listarPorId($id, $tabela, $campochaveID) {
        $this->sql = sprintf("select * from $tabela WHERE $campochaveID = %s", $id);
        $this->rs = $this->RunSelect($this->sql);
        return $this->rs;
    }

    public function setTotal() {
        $i = count($this->x);
        $this->total = $i;
    }

    public function noSql($sql, $asp = "#$") {
        $this->sql = $sql;
        $temp = str_replace("'", '"', $this->sql);
        $this->sql = str_replace($asp, "'", $temp);
        return $this->sql;
    }

    public function getSql() {
        return $this->sql;
    }

}
?>