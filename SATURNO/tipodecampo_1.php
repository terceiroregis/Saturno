<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link active show" data-toggle="tab" href="#ctodos">*</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#cinput">Ent.</a>
    </li>  
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#cselect">Sel.o</a>
    </li>  
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#carquivo">Ane.</a>
    </li>    
</ul>
<div id="myTabContent" class="tab-content">
    <div class="tab-pane fade active show" id="ctodos">
        <?php include './campos/todos.php'; ?>
    </div>
    <div class="tab-pane fade" id="cinput">
        <?php include './campos/input.php'; ?>
    </div>
    <div class="tab-pane fade" id="cselect">
        <?php include './campos/select.php'; ?>
    </div>
    <div class="tab-pane fade" id="carquivo">
        <?php include './campos/arquivo.php'; ?>
    </div>

</div>